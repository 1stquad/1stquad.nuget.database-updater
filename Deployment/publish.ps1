$version = '<version>' + [System.Diagnostics.FileVersionInfo]::GetVersionInfo("..\Sources\FirstQuad.DatabaseUpdater\bin\FirstQuad.DatabaseUpdater.dll").FileVersion + '</version>'
(Get-Content "1stquad.nuget.database-updater.nuspec") | Foreach-Object {$_ -replace "(<version>)([0-9]+).([0-9]+).([0-9]+).([0-9]+)(<\/version>)", $version} | Set-Content "1stquad.nuget.database-updater.nuspec"

Remove-Item * -Filter *.nupkg 
$nuget = Resolve-Path "nuget.exe"
&$nuget pack 1stquad.nuget.database-updater.nuspec
&$nuget push *.nupkg ecdfe2ed-e5a9-43d8-a9cd-d0789caf88d2 -s https://www.nuget.org/api/v2/package
Remove-Item * -Filter *.nupkg 
