﻿using System;

namespace FirstQuad.DatabaseUpdater
{
    public class VersionRecord
    {
        public DateTime TimeStamp { get; set; }

        public string VersionDescription { get; set; }

        public string Environment { get; set; }

        public string OriginalFileName { get; set; }

        public string SqlText { get; set; }

        public VersionStatus Status { get; set; }
    }
}