namespace FirstQuad.DatabaseUpdater
{
    public enum VersionStatus
    {
        System = 0,
        Applied = 1,
        Skipped = 2
    }
}