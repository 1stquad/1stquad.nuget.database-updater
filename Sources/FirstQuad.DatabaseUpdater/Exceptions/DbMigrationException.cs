﻿using System;
using FirstQuad.Common.Exceptions;

namespace FirstQuad.DatabaseUpdater.Exceptions
{
    public class DbMigrationException: BaseException
    {
        public string FileName { get; }

        public DbMigrationException(string fileName, string message) : base(message)
        {
            FileName = fileName;
        }
        
        public DbMigrationException(string fileName, string message,  Exception innerException) : base(message, null, innerException)
        {
            FileName = fileName;
        }
    }
}