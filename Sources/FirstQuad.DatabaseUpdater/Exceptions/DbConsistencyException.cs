﻿using System;
using FirstQuad.Common.Exceptions;

namespace FirstQuad.DatabaseUpdater.Exceptions
{
    public class DbConsistencyException : BaseException
    {
        public DbConsistencyException(string message) : base(message)
        {
        }

        public DbConsistencyException(string message, Exception innerException) : base(message, null, innerException)
        {
        }
    }
}