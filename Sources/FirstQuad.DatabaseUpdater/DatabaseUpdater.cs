﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using FirstQuad.Common.Exceptions;
using FirstQuad.DatabaseUpdater.Exceptions;

namespace FirstQuad.DatabaseUpdater
{
    public class DatabaseUpdaterOptions
    {
        public DatabaseUpdaterOptions()
        {
            CommandTimeoutSeconds = 30;
        }

        public int CommandTimeoutSeconds { get; set; } 
    }

    public static class DatabaseUpdater
    {
        private const string VersionsTableName = "DatabaseVersion";
        private const string ReadVersionInfoSql = "SELECT TimeStamp, VersionDescription, SqlText, Status FROM DatabaseVersion ORDER BY TimeStamp DESC";

        private const string AddVersionInfoSql =
            "INSERT INTO DatabaseVersion(TimeStamp, VersionDescription, SqlText, ApplyDateTime, Status) VALUES (@TimeStamp, @VersionDescription, @SqlText, @ApplyDateTime, @Status)";

        private static readonly object LockObj = new {};

        public static DatabaseUpdaterOptions Options { get; } = new DatabaseUpdaterOptions();

        #region SQLs

        private const string CreateVersionsTableSql = @"CREATE TABLE [dbo].[DatabaseVersion](
                                                          [TimeStamp] [datetime] NOT NULL,
                                                          [VersionDescription] nvarchar(max) NOT NULL,
                                                          [SqlText] nvarchar(max) NOT NULL,
                                                          [ApplyDateTime] datetime NOT NULL,
                                                          [Status] int NOT NULL,
                                                          CONSTRAINT [PK_DatabaseVersion] PRIMARY KEY CLUSTERED 
                                                          (
                                                             [TimeStamp] ASC
                                                          )
                                                       )";


        private const string EnsureStatusColumn = @"IF  NOT EXISTS(SELECT * FROM sys.columns 
                                                    WHERE Name = N'Status' AND Object_ID = Object_ID(N'dbo.DatabaseVersion'))
                                                    BEGIN				
	                                                    ALTER TABLE [dbo].[DatabaseVersion]
	                                                    ADD [Status] int NOT NULL CONSTRAINT DF_DatabaseVersion_Status DEFAULT (1)
                                                    END
                                                    ";

        private const string ConsistencyCheckSql = @"SET NOCOUNT ON;
                                                    IF OBJECT_ID('tempdb.dbo.#objects') IS NOT NULL
                                                        DROP TABLE #objects

                                                    CREATE TABLE #objects (
                                                            obj_id INT PRIMARY KEY
                                                        , obj_name NVARCHAR(261)
                                                        , err_message NVARCHAR(2048) NOT NULL
                                                        , obj_type CHAR(2) NOT NULL
                                                    )

                                                    INSERT INTO #objects (obj_id, obj_name, err_message, obj_type)
                                                    SELECT 
                                                            t.referencing_id
                                                        , obj_name = QUOTENAME(SCHEMA_NAME(o.[schema_id])) + '.' + QUOTENAME(o.name)
                                                        , 'Invalid object name ''' + t.obj_name + ''''
                                                        , o.[type]
                                                    FROM (
                                                        SELECT
                                                                d.referencing_id
                                                            , obj_name = MAX(COALESCE(d.referenced_database_name + '.', '') 
                                                                    + COALESCE(d.referenced_schema_name + '.', '') 
                                                                    + d.referenced_entity_name)
                                                        FROM sys.sql_expression_dependencies d
                                                        WHERE d.is_ambiguous = 0
                                                            AND d.referenced_id IS NULL -- если не можем определить от какого объекта зависимость
                                                            AND d.referenced_server_name IS NULL -- игнорируем объекты с Linked server
                                                            AND CASE d.referenced_class -- если не существует
                                                                WHEN 1 -- объекта
                                                                    THEN OBJECT_ID(
                                                                        ISNULL(QUOTENAME(d.referenced_database_name), DB_NAME()) + '.' + 
                                                                        ISNULL(QUOTENAME(d.referenced_schema_name), SCHEMA_NAME()) + '.' + 
                                                                        QUOTENAME(d.referenced_entity_name))
                                                                WHEN 6 -- или типа данных
                                                                    THEN TYPE_ID(
                                                                        ISNULL(d.referenced_schema_name, SCHEMA_NAME()) + '.' + d.referenced_entity_name) 
                                                                WHEN 10 -- или XML схемы
                                                                    THEN (
                                                                        SELECT 1 FROM sys.xml_schema_collections x 
                                                                        WHERE x.name = d.referenced_entity_name
                                                                            AND x.[schema_id] = ISNULL(SCHEMA_ID(d.referenced_schema_name), SCHEMA_ID())
                                                                        )
                                                                END IS NULL
                                                        GROUP BY d.referencing_id
                                                    ) t
                                                    JOIN sys.objects o ON t.referencing_id = o.[object_id]
                                                    WHERE LEN(t.obj_name) > 4 -- чтобы не показывать валидные алиасы, как невалидные объекты

                                                    DECLARE
                                                            @obj_id INT
                                                        , @obj_name NVARCHAR(261)
                                                        , @obj_type CHAR(2)

                                                    DECLARE cur CURSOR FAST_FORWARD READ_ONLY LOCAL FOR
                                                        SELECT
                                                                sm.[object_id]
                                                            , QUOTENAME(SCHEMA_NAME(o.[schema_id])) + '.' + QUOTENAME(o.name)
                                                            , o.[type]
                                                        FROM sys.sql_modules sm
                                                        JOIN sys.objects o ON sm.[object_id] = o.[object_id]
                                                        LEFT JOIN (
                                                            SELECT s.referenced_id
                                                            FROM sys.sql_expression_dependencies s
                                                            JOIN sys.objects o ON o.object_id = s.referencing_id
                                                            WHERE s.is_ambiguous = 0
                                                                AND s.referenced_server_name IS NULL
                                                                AND o.[type] IN ('C', 'D', 'U')
                                                            GROUP BY s.referenced_id
                                                        ) sed ON sed.referenced_id = sm.[object_id]
                                                        WHERE sm.is_schema_bound = 0 -- объект создан без опции WITH SCHEMABINDING
                                                            AND sm.[object_id] NOT IN (SELECT o2.obj_id FROM #objects o2) -- чтобы повторно не определять невалидные объекты
                                                            AND OBJECTPROPERTY(sm.[object_id], 'IsEncrypted') = 0
                                                            AND (
                                                                    o.[type] IN ('IF', 'TF', 'V', 'TR')
                                                                -- in rare cases , sp_refreshsqlmodule can spoil the metadata stored procedures (Bug # 656863 )
                                                                OR o.[type] = 'P' 
                                                                OR (
                                                                        o.[type] = 'FN'
                                                                    AND
                                                                        -- игнорируем скалярные функции, которые используются в DEFAULT/CHECK констрейнтах и в COMPUTED столбцах
                                                                        sed.referenced_id IS NULL
                                                                )
                                                            )

                                                    OPEN cur

                                                    FETCH NEXT FROM cur INTO @obj_id, @obj_name, @obj_type

                                                    WHILE @@FETCH_STATUS = 0 BEGIN

                                                        BEGIN TRY
                                                            EXEC sys.sp_refreshsqlmodule @name = @obj_name, @namespace = N'OBJECT' 
                                                        END TRY
                                                        BEGIN CATCH
                                                            INSERT INTO #objects (obj_id, obj_name, err_message, obj_type) 
                                                            SELECT @obj_id, @obj_name, ERROR_MESSAGE(), @obj_type
                                                        END CATCH

                                                        FETCH NEXT FROM cur INTO @obj_id, @obj_name, @obj_type

                                                    END

                                                    CLOSE cur
                                                    DEALLOCATE cur

                                                    SELECT obj_name, err_message, obj_type
                                                    FROM #objects";

        #endregion

        private static List<VersionRecord> ReadFolderRecords(string folderName, string executionEnvironment)
        {
            var fullFolderName = string.IsNullOrEmpty(folderName) ? Environment.CurrentDirectory : Path.Combine(Environment.CurrentDirectory, folderName);
            var files = Directory.GetFiles(fullFolderName, "*.sql");

            var versionRecords = new List<VersionRecord>();

            foreach (var fullFileName in files)
            {
                var fileName = Path.GetFileName(fullFileName);
                if (string.IsNullOrEmpty(fileName))
                    throw new InvalidProgramException($"Can't get file name for item '{fullFileName}'");

                if (fileName.ToUpper() == "INIT.SQL")
                    continue;
                if (fileName.ToUpper() == "MIGRATE.SQL")
                    continue;

                var exactTimestamp = "yyyyMMddHHmmss";
                var splitterIndex = fileName.IndexOf('_');
                var numberSubstring = fileName.Substring(0, exactTimestamp.Length);
                var descriptionText = fileName.Substring(splitterIndex + 1);
                var environment = fileName.Substring(exactTimestamp.Length, fileName.IndexOf('_') - exactTimestamp.Length).ToLower();

                var shouldApplyScript = environment == "diff" || environment.Contains(executionEnvironment.ToLower());

                versionRecords.Add(new VersionRecord
                {
                    TimeStamp = DateTime.ParseExact(numberSubstring, exactTimestamp, null, DateTimeStyles.AssumeLocal),
                    VersionDescription = descriptionText,
                    SqlText = File.ReadAllText(fullFileName),
                    OriginalFileName = fileName,
                    Environment = environment,
                    Status = shouldApplyScript ? VersionStatus.Applied : VersionStatus.Skipped
                });
            }

            return versionRecords;
        }

        private static void RunScript(SqlConnection connection, SqlTransaction transaction, VersionRecord record)
        {
            var regex = new Regex(@"\r{0,1}\n[ \t]{0,}GO[ \t]{0,}\r{0,1}\n", RegexOptions.IgnoreCase);
            var commands = regex.Split(Environment.NewLine + record.SqlText + Environment.NewLine).ToList();

            var defaultOptions = "SET ANSI_NULLS ON\r\n  SET ANSI_PADDING ON\r\n  SET ANSI_WARNINGS ON\r\n  SET QUOTED_IDENTIFIER ON\r\n  SET ANSI_NULL_DFLT_ON ON";
            commands.Insert(0, defaultOptions);

            for (int i = 0; i < commands.Count; i++)
            {
                if (commands[i] != string.Empty)
                {
                    using (SqlCommand command = new SqlCommand(commands[i], connection, transaction))
                    {
                        command.CommandTimeout = Options.CommandTimeoutSeconds;
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            throw new DbMigrationException(record.OriginalFileName, $"Execution of SQL {record.OriginalFileName} failed: {commands[i]}", ex);
                        }
                    }
                }
            }
        }

        private static void AddVersion(SqlConnection connection, SqlTransaction transaction, VersionRecord versionRecord, VersionStatus status)
        {
            using (SqlCommand command = new SqlCommand(AddVersionInfoSql, connection, transaction))
            {
                command.Parameters.Add("@TimeStamp", SqlDbType.DateTime).Value = versionRecord.TimeStamp;
                command.Parameters.Add("@VersionDescription", SqlDbType.Text).Value = versionRecord.VersionDescription;
                command.Parameters.Add("@SqlText", SqlDbType.Text).Value = versionRecord.SqlText;
                command.Parameters.Add("@ApplyDateTime", SqlDbType.DateTime).Value = DateTime.UtcNow;
                command.Parameters.Add("@Status", SqlDbType.Int).Value = (int)status;
                command.ExecuteNonQuery();
            }
        }

        public static IList<string> ListTableNames(SqlConnection connection)
        {
            var result = new List<string>();
            using (var command = new SqlCommand("SELECT name FROM sys.Tables", connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                        result.Add(reader["name"].ToString());
                    return result;
                }
            }
        }

        private static void ExecuteNonQuery(string sql, SqlConnection connection, SqlTransaction transaction)
        {
            using (SqlCommand command = new SqlCommand(sql, connection, transaction))
            {
                command.ExecuteNonQuery();
            }
        }

        private static void CreateVersionsTable(SqlConnection connection, SqlTransaction transaction)
        {
            ExecuteNonQuery(CreateVersionsTableSql, connection, transaction);
        }

        private static void EnsureStatusColumnInVersionsTable(SqlConnection connection, SqlTransaction transaction)
        {
            ExecuteNonQuery(EnsureStatusColumn, connection, transaction);
        }

        private static string GetConnectionString()
        {
            var scriptsConnectionString = ConfigurationManager.AppSettings["ScriptsConnectionString"];
            if (string.IsNullOrEmpty(scriptsConnectionString))
                throw new InvalidConfigurationException("ScriptsConnectionString property is not defined in web.config");

            var efConn = new EntityConnectionStringBuilder(ConfigurationManager.ConnectionStrings[scriptsConnectionString].ConnectionString);
            return efConn.ProviderConnectionString;
        }

        private static SqlConnection GetConnection()
        {
            var connection = new SqlConnection(GetConnectionString());
            try
            {
                connection.Open();
                return connection;
            }
            catch
            {
                connection.Dispose();
                throw;
            }
        }

        private static string GetScriptsDirectory(string rootDirectory)
        {
            var scriptsDirectory = ConfigurationManager.AppSettings["ScriptsDirectory"];
            if (string.IsNullOrEmpty(scriptsDirectory))
                throw new InvalidConfigurationException("ScriptsDirectory property is not defined in web.config");

            scriptsDirectory = Path.Combine(rootDirectory, scriptsDirectory);
            if (!Directory.Exists(scriptsDirectory))
                throw new InvalidConfigurationException($"Can't find {scriptsDirectory} that should contain migrate scripts");

            return scriptsDirectory;
        }

        public static List<VersionRecord> GetAvailableScripts(string rootDirectory)
        {
            var environment = ConfigurationManager.AppSettings["Environment"];
            if (string.IsNullOrEmpty(environment))
                throw new InvalidConfigurationException("Environment property is not defined in web.config");

            var fileVersionRecords = ReadFolderRecords(GetScriptsDirectory(rootDirectory), environment);

            return fileVersionRecords;
        }

        private static void EnsureSchema(string rootDirectory)
        {
            IList<string> tables;
            using (var connection = GetConnection())
            {
                tables = ListTableNames(connection);
            }

            if (!tables.Contains(VersionsTableName))
            {
                ApplyInitializationScript(!tables.Any(), rootDirectory, CreateVersionsTable);
            }

            using (var connection = GetConnection())
            {
                EnsureStatusColumnInVersionsTable(connection, null);
            }
        }

        public static List<VersionRecord> GetNewScripts(string rootDirectory)
        {
            EnsureSchema(rootDirectory);

            var dbVersionRecords = new List<VersionRecord>();
            var fileVersionRecords = GetAvailableScripts(rootDirectory);

            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand(ReadVersionInfoSql, connection))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var versionRecord = new VersionRecord
                            {
                                TimeStamp = dataReader.GetDateTime(0),
                                VersionDescription = dataReader.GetString(1),
                                SqlText = dataReader.GetString(2),
                                Status = (VersionStatus)dataReader.GetInt32(3),
                            };
                            dbVersionRecords.Add(versionRecord);
                        }
                    }
                }

                return fileVersionRecords.Where(fv => dbVersionRecords.All(dv => dv.TimeStamp != fv.TimeStamp)).ToList();
            }
        }

        private static void ApplySystemScript(string rootDirectory, string fileName, string description, Action<SqlConnection, SqlTransaction> initAction = null)
        {
            var scriptsDirectory = GetScriptsDirectory(rootDirectory);
            
            var initFile = Path.Combine(scriptsDirectory, fileName);
            if (File.Exists(initFile))
            {
                var initSql = File.ReadAllText(initFile);
                var version = new VersionRecord
                {
                    SqlText = initSql,
                    OriginalFileName = fileName,
                    TimeStamp = DateTime.UtcNow,
                    VersionDescription = description,
                    Status = (int) VersionStatus.System
                };
                MigrateVersionUp(version, initAction);
            }
            else
            {
                using (var connection = GetConnection())
                {
                    using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                    {
                        initAction?.Invoke(connection, transaction);
                        transaction.Commit();
                    }
                }
            }
        }

        private static void ApplyInitializationScript(bool cleanDatabase, string rootDirectory, Action<SqlConnection, SqlTransaction> initAction = null)
        {
            if (cleanDatabase)
            {
                ApplySystemScript(rootDirectory, "init.sql", "Initialization Script", initAction);
            }
            else
            {
                ApplySystemScript(rootDirectory, "migrate.sql", "Migration Script", initAction);
            }
        }

        private static void MigrateVersionUp(VersionRecord version, Action<SqlConnection, SqlTransaction> initAction = null)
        {
            using (var connection = GetConnection())
            {
                using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        initAction?.Invoke(connection, transaction);

                        if (version.Status != VersionStatus.Skipped)
                        {
                            RunScript(connection, transaction, version);
                        }

                        AddVersion(connection, transaction, version, version.Status);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch (Exception rollbackEx)
                        {
                            throw new AggregateException($"Can't rollback changes because of: \"{ex.Message}\" and \"{rollbackEx.Message}\"", ex, rollbackEx);
                        }
                        throw;
                    }
                }
            }
        }

        public static void UpdateDatabase(string rootDirectory)
        {
            lock (LockObj)
            {
                var newVersions = GetNewScripts(rootDirectory);
                foreach (var newVersion in newVersions.OrderBy(x => x.TimeStamp))
                {
                    MigrateVersionUp(newVersion);
                }
                CheckConsistency();
            }
        }

        public static void CheckConsistency()
        {
            using (var connection = GetConnection())
            {
                var ignoreObjects = new[]
                {
                    "[sys].[database_firewall_rules]",
                    "[dbo].[sp_upgraddiagrams]"
                };

                using (SqlCommand command = new SqlCommand(ConsistencyCheckSql, connection))
                {
                    try
                    {
                        var errors = new List<string>();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                var objectName = reader.GetString(reader.GetOrdinal("obj_name"));
                                var errorMessage = reader.GetString(reader.GetOrdinal("err_message"));

                                if (!ignoreObjects.Contains(objectName))
                                {
                                    errors.Add($"{objectName} {errorMessage}");
                                }
                            }
                        }

                        if (errors.Any())
                        {
                            throw new DbConsistencyException($"Consistency SQL check failed: {string.Join(Environment.NewLine, errors)}");
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new DbConsistencyException($"Consistency SQL check failed: {ex.Message}", ex);
                    }
                }
            }
        }
    }
}